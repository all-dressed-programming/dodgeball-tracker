{
  description = "Pre-built CPU-only OpenCV packages for Python.  ";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    opencv-python-related.url =
      "gitlab:all-dressed-programming/opencv-python-related";
    opencv-python-related.inputs.nixpkgs.follows = "nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  nixConfig = {
    extra-substituters = [ "https://cuda-maintainers.cachix.org" ];
    extra-trusted-public-keys = [
      "cuda-maintainers.cachix.org-1:0dq3bujKpuEPMCX6U4WylrUDZ9JyUG0VpVZa7CNfq5E="
    ];
  };

  outputs = { self, nixpkgs, flake-utils, opencv-python-related }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          system = system;
          config.allowUnfree = true;
        };

        pythonPackages = pkgs.python312Packages
          // opencv-python-related.packages.${system}.python312Packages;

        pandera = pkgs.python3Packages.buildPythonPackage  {
          version = "0.5.11";
          pname = "lapx";
          pyproject = true;
          src = pkgs.python3Packages.fetchPypi {
            pname = "pandera";
            version = "0.20.4";
            hash = "sha256-zPYXgpPvnUOT3Ed25HR349LdUcgA7N/t7Gf/9Q9K08I=";

          };

          propagatedBuildInputs = with pythonPackages; [ multimethod
            numpy
            pandas
            pydantic
            typeguard
            typing-inspect
            wrapt
           ];

          nativeBuildInputs = with pythonPackages; [
            setuptools
          ];

          meta = with pkgs.lib; {
            description = "The Open-source Framework for Precision Data Testing";
            homepage = "https://github.com/unionai-oss/pandera";
            license = pkgs.lib.licenses.mit;
            maintainers = with maintainers; [ ];
          };
        };

        virtual-env = pkgs.python312.withPackages (ps: [
          ps.matplotlib
          ps.torchWithCuda
          ps.jupyter
          ps.pytest
          pythonPackages.opencv4
          pythonPackages.boxmot
          pythonPackages.ultralytics
          pythonPackages.click
          pandera
        ]);

      in {
        packages = { env = virtual-env; pandera = pandera; };
        devShells.default = pkgs.mkShell {
          buildInputs = [ virtual-env pkgs.yt-dlp pkgs.ffmpeg_7 ];
        };
      });
}
