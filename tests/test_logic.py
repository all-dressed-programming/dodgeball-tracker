import numpy as np
import pandas as pd
import pandera as pa
import pytest

from dodgeballtracker.logic import BirdViewCreator
from dodgeballtracker.typing import TrackedDetectedObject


@pytest.fixture
def view_perspective() -> np.matrix:
    return np.matrix([
        [200, 100, 0],
        [100, 100, 0],
        [0, 0, 1]
    ])


@pytest.fixture
@pa.check_io(out=TrackedDetectedObject)
def mock_data() -> pd.DataFrame:
    return pd.DataFrame(
        {
            "xmin": pd.Series(dtype='float64', data=[10.0, 100.0]),
            "ymin": pd.Series(dtype='float64', data=[30.0, 130.0]),
            "xmax": pd.Series(dtype='float64', data=[55.0, 150.0]),
            "ymax": pd.Series(dtype='float64', data=[65.0, 195.0]),
            "score": pd.Series(dtype='float64', data=[0.66, 0.56]),
            "category": pd.Series(dtype='float64', data=[3, 3]),
            "object_id": pd.Series(dtype='float64', data=[73232, 837276]),
            "index": pd.Series(dtype='float64', data=[0, 1]),
        }
    )


def test_get_bird_view(view_perspective: np.matrix,
                       mock_data: pd.DataFrame) -> None:
    bvd = BirdViewCreator(view_perspective)
    ret = bvd.append_bird_view(mock_data)
