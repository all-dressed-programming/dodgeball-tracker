import contextlib
import time
from typing import Iterator
from dodgeballtracker.typing import VideoFrame, VideoTimestamp
import cv2
import logging

logger = logging.getLogger(__name__)



def iter_frames(video_cap) -> Iterator[tuple[VideoFrame, VideoTimestamp]]:
    while 1:
        ret, frame = video_cap.read()
        if not ret:
            return
        yield frame, video_cap.get(cv2.CAP_PROP_POS_MSEC)


@contextlib.contextmanager
def time_tracker():
    start = time.monotonic()
    yield
    end = time.monotonic()
    logger.debug(f"Time to process 1 frame: {(end-start) * 1000:.0f} milliseconds")
