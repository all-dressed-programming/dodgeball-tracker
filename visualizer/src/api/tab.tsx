import {MatchInfo} from "../services/matches.ts";

export class ViewTabService {

    infoTab(matchInfo: MatchInfo){
        return (<table class="table is-bordered is-striped is-fullwidth">
            <tbody>
            <tr>
                <td>Label</td>
                <td>{matchInfo.label}</td>
            </tr>
            <tr>
                <td>Id</td>
                <td>{matchInfo.id}</td>
            </tr>
            <tr>
                <td>Date</td>
                <td>{matchInfo.matchDate}</td>
            </tr>
            <tr>
                <td>Video URL</td>
                <td>
                    <a href={matchInfo.originalVideoUrl}>{matchInfo.originalVideoUrl}</a>
                    </td>
            </tr>
            </tbody>

        </table>)

    }

    playerTab(matchInfo: MatchInfo){
        return (<div>
            <div id="foo"></div>
            <form hx-swap="innerHTML"
                  hx-get={"/api/v1/match/:id/pane/player/data"}
                  hx-target="#court-content">
                <div class="field is-horizontal">
                  <div class="field-label is-normal">
                    <label class="label">Min Time</label>
                  </div>
                  <div class="field-body">
                    <div class="field">
                      <div class="control">
                        <input class="input is-danger" type="range" min="0" max="100" />
                      </div>
                    </div>
                  </div>
                </div>

                <div class="field is-horizontal">
                  <div class="field-label is-normal">
                    <label class="label">Max Time</label>
                  </div>
                  <div class="field-body">
                    <div class="field">
                      <div class="control">
                        <input class="input is-danger" type="range" min="0" max="100" />
                      </div>
                    </div>
                  </div>
                </div>

                 <div class="field">
                      <div class="control">
                          <button class="button"> Show </button>
                      </div>
                  </div>
                </form>
        </div>)

    }

}