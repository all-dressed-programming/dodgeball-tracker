import {Router} from "https://deno.land/x/oak/mod.ts";
import {FileMatchService} from "../services/matches.ts";
import {ViewTabService} from "./tab.tsx";


export const apiRouter = new Router( {prefix: '/api/v1'});

async function getMatchService(): Promise<FileMatchService> {
    const f = "/home/didier/all-dressed-programming/dodgeball-tracker/outputs/index.json"
    return new FileMatchService(f)
}

apiRouter.get('/ping', (ctx) => {
    ctx.response.body = <span>pong</span>
})

apiRouter.get("/match/:id/title", async function(ctx){
    const service = await getMatchService()
    const match = await service.getMatchInfoById(ctx.params.id)
    ctx.response.body = (<span>Match: {match.label}</span>)
});

apiRouter.get("/match/:id/pane/info", async function(ctx){
    const service = await getMatchService()
    const match = await service.getMatchInfoById(ctx.params.id)
    const viewService = new ViewTabService();
    ctx.response.body = viewService.infoTab(match)
});

apiRouter.get("/match/:id/pane/player", async function(ctx){
    const service = await getMatchService()
    const match = await service.getMatchInfoById(ctx.params.id)
    const viewService = new ViewTabService();
    ctx.response.body = viewService.playerTab(match)
});

apiRouter.get("/match/:id/pane/player/data", async function(ctx){
    ctx.response.body = <rect width="100" height="100"/>
});

apiRouter.get('/list-matches', async (ctx) => {
    const matchService = await getMatchService()
    const rows = [];
    for await (const match of matchService.getAllMatchInfo()){
        rows.push(
            <tr>
                <td><a href={`/match/${match.id}/info`}>{ match.label }</a></td>
                <td><a href={match.originalVideoUrl}>{ match.originalVideoUrl }</a></td>
            </tr>
        )
    }
    ctx.response.body = (<div>
        <table class="table is-bordered is-striped is-fullwidth">
            <thead>
            <tr>
                <th>Label</th>
                <th>Video Link</th>
            </tr>
            </thead>
            <tbody>
            {rows}
            </tbody>
        </table>
    </div>)
})
