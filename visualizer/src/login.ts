import { Router } from "https://deno.land/x/oak/mod.ts";


export const loginRouter = new Router();

export function isLoggedIn(ctx){
    return ctx.request.headers.get('dgt-session')
}


loginRouter.post("/api/v1/login", async (ctx)=> {
    const username = (await ctx.request.body().value).get("username");
    ctx.response.headers.append("HX-Trigger", JSON.stringify({'login': username}));
    ctx.response.body = username;
});

loginRouter.post("/api/v1/logout", (ctx)=> {
    ctx.response.headers.append("HX-Trigger", "logout");
    ctx.response.body = "";
});





