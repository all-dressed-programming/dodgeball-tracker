import { Application } from "https://deno.land/x/oak/mod.ts";
import staticFiles from "https://deno.land/x/static_files@1.1.6/mod.ts";
import {loginRouter, isLoggedIn} from "./login.ts";
import {apiRouter} from "./api/mod.tsx";
import {viewRouter} from "./views/mod.tsx";
import {renderToString} from "https://esm.sh/preact-render-to-string@5.1.19?deps=preact@10.5.15";
import Logger from "https://deno.land/x/logger@v1.1.5/logger.ts";

const logger = new Logger();
export const app = new Application();

function isReactComponent(x){
    return !!x && Object.hasOwn(x, "type") && Object.hasOwn(x, "props")
}

async function reactRendererMiddleware(ctx, next){
     await next();
    if(isReactComponent(ctx.response.body)){
        logger.info("Converting react component to string.")
        ctx.response.body  = renderToString(ctx.response.body)

    }
}

async function loggingMiddleware(ctx, next){
     if(!isLoggedIn(ctx)){
        logger.info("user not logged in.")
        ctx.response.headers.append("HX-Redirect", "/login.html");
        return
    }
    logger.info('user logged in')
    await next();
}


app.use(loginRouter.routes());
app.use(viewRouter.routes());
app.use(staticFiles("public", {brotli: true, fetch: true}));
app.use(loggingMiddleware)
app.use(reactRendererMiddleware)
app.use(apiRouter.routes());




