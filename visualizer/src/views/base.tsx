export function Head(){
    return (<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>JS Reading App</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.4/css/bulma.min.css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/highlightjs/cdn-release@11.9.0/build/styles/default.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.9.0/styles/atom-one-light.min.css"/>

    <script src="/vendor/htmx/htmx.js"></script>
  </head>)
}


export function NavBar(){
    return (<nav class="navbar" role="navigation" aria-label="main navigation">
        <div class="navbar-menu is-active">
            <div class="navbar-start">
                <span class="navbar-item">
                    <a href="/">
                        <img src="/images/logo.png" alt="js logo" style="height: 100%"/>
                    </a>
                </span>
            </div>

            <div class="navbar-end">
                <a class="navbar-item" href="#" hx-post="/api/v1/logout">
                    Way Out
                </a>
            </div>
        </div>
    </nav>)
}

export function Body({pathVariables, children}){
    const urlPlaceHolders = {
        pathVariables: Object.fromEntries(Object.entries(pathVariables ?? {}).map(([k, v]) => [`:${k}`, v]))
    }
    return (<body>
    { children }
    <script src="/scripts/auth.js" type="module"> </script>
    <script
        type="json"
        id="url-placeholders"
        dangerouslySetInnerHTML={{__html: JSON.stringify(urlPlaceHolders, null, 4)}}></script>
    </body>)
}