import { Router } from "https://deno.land/x/oak/mod.ts";
import {renderToString} from "https://esm.sh/preact-render-to-string@5.1.19?deps=preact@10.5.15";
import * as base from './base.tsx';
import {Head} from "./base.tsx";
export const viewRouter = new Router();

function renderPageToString(page){
    return `<!DOCTYPE html>${renderToString(page)}`
}


function Court(){
    const margin = 50;
    const returnLine = {stroke: 'blue', strokeWidth: '2px'}
    const courtBorder = {stroke: 'red',  strokeWidth: '4px', fill: 'gray' }
    const lines = [
        <line x1={450} x2={450} y1={0} y2={450} style={courtBorder}></line>,
        <line x1={300} x2={300} y1={0} y2={450} style={returnLine}></line>,
        <line x1={600} x2={600} y1={0} y2={450} style={returnLine}></line>,
    ]
    return (<svg class="court-side"
                 xmlns="http://www.w3.org/2000/svg"
                 style={{width: 900 + 2 * margin, height: 450 + 2 * margin, backgroundColor: 'pink'}}>
        <g class="court-border" id="court" transform = {`translate(${margin} ${margin})`}>
            <rect width={900} height={450} style={courtBorder}/>
            { lines }
            <g id="court-content"></g>
        </g>
    </svg>)
}

viewRouter.get("/match/:id/:tab", (ctx) => {
    const page = (<html lang="en">
    <base.Head/>
    <base.Body pathVariables={ctx.params}>
        <base.NavBar></base.NavBar>
        <div class="columns is-centered">

            <div class="column is-two-thirds">
                <div class="columns">
                    <div class="column">
                        <h4 class="title is-4"
                            hx-get="/api/v1/match/:id/title"
                            hx-trigger="load"> &nbsp;  </h4>
                    </div>
                </div>

                <div class="columns">
                    <div class="column" style={{textAlign: "center"}}>
                        <Court></Court>
                    </div>
                </div>

                <div class="columns">
                    <div class="column" style={{textAlign: "center"}}>
                        <div class="tabs">
                          <ul>
                            <li className={ctx.params.tab === 'info' ? "is-active": ""}>
                                <a href={`/match/${ctx.params.id}/info`}>Information</a>
                            </li>
                            <li className={ctx.params.tab === 'player' ? "is-active": ""}>
                                <a href={`/match/${ctx.params.id}/player`}>Player</a>
                            </li>
                          </ul>
                        </div>
                    </div>
                </div>
                <div class="columns">
                    <div class="column"
                         hx-get={`/api/v1/match/${ctx.params.id}/pane/${ctx.params.tab}`}
                         hx-trigger="load">
                        foo bar toto
                    </div>
                </div>
            </div>
        </div>
    </base.Body>
    </html>)
    ctx.response.body = renderPageToString(page)
})


viewRouter.get("/index.html", (ctx) => {
    const page = (<html lang="en">
    <base.Head/>
    <base.Body>
        <base.NavBar></base.NavBar>
        <div class="columns is-centered">
            <div class="column is-two-thirds"
                 id="main-block"
                 hx-get="/api/v1/list-matches"
                 hx-trigger="load">
            </div>
        </div>
    </base.Body>
    </html>)
    ctx.response.body = renderPageToString(page)
})

viewRouter.redirect("/", "/index.html")

viewRouter.get("/login.html", (ctx) => {
    const page = (<html lang="en">
    <base.Head/>
    <base.Body>
        <base.NavBar></base.NavBar>
        <div class="columns is-centered">
          <div class="column is-half">
            <form hx-post="/api/v1/login" hx-swap="none">

              <div class="field has-addons">
                <div class="control">
                  <input class="input" id="username" type="text" placeholder="bob.l.eponge@ocean.com" name="username" required />
                </div>
                <div class="control">
                  <input type="submit" class="button" value="Login" />
                </div>
              </div>
            </form>
          </div>
        </div>
    </base.Body>
    </html>)
    ctx.response.body = renderPageToString(page)
})
