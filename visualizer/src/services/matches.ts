
export type MatchInfo = {
      id: string,
      originalVideoUrl: string,
      matchDate: string,
      label: string
}

export interface MatchService {

    getAllMatchInfo(): AsyncIterator<MatchInfo>;
    getMatchInfoById(id: string): Promise<MatchInfo|undefined>;

}

export class FileMatchService implements MatchService{

    readonly #indexFile: string

    constructor(indexFile: string) {
        this.#indexFile = indexFile;
    }

    async *getAllMatchInfo(): AsyncIterator<MatchInfo>{
        const data = await Deno.readTextFile(this.#indexFile)
        const ret = JSON.parse(data) as {matches: MatchInfo[]}
        yield* ret.matches
    }

    async getMatchInfoById(id: string): Promise<MatchInfo|undefined>{
        for await(const mi of this.getAllMatchInfo()){
            if(mi.id === id){
                return mi
            }
        }
    }




}