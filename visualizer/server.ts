import {app} from "./src/server.ts";
import Logger from "https://deno.land/x/logger@v1.1.5/logger.ts";



const logger = new Logger()
const port = 8000
logger.info(`Starting web server on port ${port}`)
await app.listen({ port });