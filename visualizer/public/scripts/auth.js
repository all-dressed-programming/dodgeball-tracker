const sessionLocalStorageKey = 'dgt-session';

let cachedPathVariables = null;

function getPathVariables(){
    if (cachedPathVariables !== null) {
        return cachedPathVariables;
    }
    const raw = document.getElementById("url-placeholders")?.textContent || "{}"
    cachedPathVariables = Object.entries(JSON.parse(raw)?.pathVariables || {})
    return cachedPathVariables

}

window.addEventListener('login', function (evt) {
    localStorage.setItem(sessionLocalStorageKey, evt.detail.value);
    document.location.href = "/";
});

window.addEventListener('logout', function (evt) {
    window.localStorage.removeItem(sessionLocalStorageKey);
    document.location.href = "/login.html";
});

window.addEventListener('htmx:configRequest', function(evt) {
    evt.detail.headers['dgt-session'] = localStorage.getItem(sessionLocalStorageKey);

    function reducer(path, [key, value]){
        return path.replace(key, value)
    }

    evt.detail.path = getPathVariables().reduce(reducer, evt.detail.path)
});
